from django.apps import AppConfig


class Story4AppConfig(AppConfig):
    name = 'Story4App'
