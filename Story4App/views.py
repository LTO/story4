from django.shortcuts import render
from django.http import HttpResponse



def index(request):
    return render(request, 'Homepage.html')

def profil(request):
    return render(request, 'Profil.html')

def Keahlian(request):
    return render(request, 'Keahlian.html')

def Pengalaman(request):
    return render(request, 'Pengalaman.html')

def Kontak(request):
    return render(request, 'Kontak.html')

def pageTambahan(request):
    return render(request, 'pageTambahan.html')