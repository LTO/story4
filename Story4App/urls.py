from django.urls import path
from . import views

app_name = 'Story4App'

urlpatterns = [
    path('', views.index, name='Homepage'),
    path('profil/', views.profil, name='profil'),
    path('keahlian/', views.Keahlian, name='Keahlian'),
    path('pengalaman/', views.Pengalaman, name='Pengalaman'),
    path('kontak/', views.Kontak, name='Kontak'),
    path('pageTambahan/', views.pageTambahan, name='pageTambahan'),
]